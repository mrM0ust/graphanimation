# draw.pl

## Stručný popis

- Program vytvoří animovaný graf ze vstupních dat. Animace je řízená parametry **(viz man)**.
- Vstupem je soubor nebo url
- Výstupem je *mp4* soubor

### Animace

Hlavním kouzlem tohoto programu je samotná animace. Rozdělí vstupní data do několika částí a animace grafu má vizuálně několik počátků.
Počet takových startovních bodů se definuje pomocí *--EffectParam "split=n"*, kde n celé nezáporné číslo (pokud n je 0, vytvoří se celiství graf).
Dalším zajímavým parameterm je *--EffectParam "reverse=1"*, který otočí směr běhu grafu.

## Popis logické stavby programu

**Kód je precizně komentovaný, popisuju jen hlavní kroky**

1. Pomocí modulu *Getopt::Long* se načtou parametry zadané na příkazové řádce do *%opts*
2. Pokud byl nadefinován konfigurační soubor, pokusím se jej načíst do hashe s defaultníma hodnotama (můžou se přepsat).
    - *load_conf_file($opts{'configfile'})*
3. Dojde ke spojení zadaných parametrů, nejnižší prioritu mají defautlní, pak ty definové v konfiguračním souboru. Nejvýznamnější jsou parametry z příkazové řádky.
    - *merge_opts(\%opts)*, do *%opts* se doplní defaultní parametry, respektive parametry z konfiguračního souboru
4. URL se stáhne a otestuje, všechny datové vstupy se spojí do jednoho chronologicky seřazeného souboru. Se datové extrémy a počet řádků.
    - *merge_args(\@ARGV, $opts{'timeformat'})* vrátí *$dataFile, $lineCnt, $xMin, $xMax, $yMin, $yMax* v tomto pořadí
    - *merge_args* volá *fetch_url*, ta rozhodne jestli je zadaný argument url, či soubor. V případě url se soubor stáhne a vrátí se jeho jméno
    - *merge_args* volá *sort_files*, ta řadí podle epochtime první řádky (vzužívá funkci *verify_file*, která vrátí epoch time první řádky).
6. Hodnota *splitCnt* udává z kolika počátečních bodů se startuje vykreslování grafu. Pro *n* bodů je potřeba rozdělit soubor na *2n* podsouborů
    - *@dataFiles = split_file($dataFile, $lineCnt, $splitCnt)* vrací jména podsouborů se stejným počtem řádků
7. Podle parametru *reverse* se inverují buď sudé nebo liché podsoubory.
    - k tomu slouží funkce *reverse_file*
8. Následuje logika pro výpočet *speed, fps a time*. Pokud je zadán jen *time*, bere se defautlní *fps* a *speed* se dopočítává. Jsou brány v úvahu všechny možné kombinace zadaných parametrů.

```
speed = #lines / (splitCnt * time * fps)
```

9. Přes pipe se spustí *gnuplot* a podle parametrů se odifuje co se má poslat.  
10. Vytvoření *mp4* animace


```bash
ffmpeg -y -framerate $fps -i $tmpDir/out_%0${n}d.png $outDir/anim.mp4
```
12. Pokud byl program spuštěn s *ending=N*, poslední frame videa se protáhne na N sekund
11. Pokud byl program spuštěn s parametrem *-E*, ignorují se chyby se závažností menší než 10

### Tento adresář

```
draw.pl -> obsahuje hlavní kód
lib/draw_lib.pm -> obsahuje funkce, které jsou z draw.pl volány
tests/conf -> ukázkové konfigurační soubory
tests/data -> nějaká data
test.sh -> ukázky spouštení programu
draw_man -> uživatelský manuál
README.md
```
